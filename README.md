## GOGOAPPS - URL collector

## Discalimer
Name of repo should be `S2FtaWwgR29nb0FwcHMgTkFTQQ==` but Gitlab does not support `=` char in repo name :)

--- 

Repository is a recruitment task from GOGOAPPS company. 

## Context
Let’s say GogoApps is building an awesome space visualization tool. In order to accomplish this task
a beautiful database of space pictures and videos are required. Such database is going to be built
using two independent microservices:

* A microservice that is able to download media files from provided urls and store them onto
some internal storage (called **media-downloader**)
  
* A microservice that would prepare a list of urls for the first microservice to use (called url-
**collector**)

While your teammates are building the media-downloader, your task is to create the latter - the url-

collector. You will be building a microservice responsible for gathering image URLs from the open
**NASA's APOD API**.

## Place for discussion
> What if we were to change the NASA API to some other images provider?

Changing provider to something else is a piece of cake. Only thing to do is implement new provider
(that implements `ImageProvider` interface) and pass new provider to `Collector`.

> What if, apart from using NASA API, we would want to have another microservice fetching urls
from European Space Agency. How much code could be reused?

Only thing that would be changed is a provider. On the other hand we could create single
pipeline for processing `tasks`. In that scenario task would need some type of identification
that says which provider to use. In `worker` would check that identification, pick proper, registered
provider that perform that task using this provider.

POC:
```go
package poc
type task struct {
	...
	taskType taskType (NASA, EAS, ...)
	...
}

func (c collector) registerProvider(t taskType, p Provider) {
	c.providerRegistry[t] = p
}

func (c collector) workerFn(...) {
	...
	var t task
	c.providerRegistry[t.taskType].Get(...)
	...
}

```

> What if we wanted to add some more query params to narrow down lists of urls - for example,
selecting only images taken by certain person. (field copyright in the API response)

First of all I would add lib that provides easy params parsing like: https://github.com/go-playground/form to speed up
the development process. Later on I will check if (in that case) NASA API provides some kind of filtering on those fields.
At the very end I'll create my own implementation. We would need to add another fields to `Image` struct.
Final version would look like that:

```go
type Image struct {
    Copyright      string `json:"copyright"`
    Date           string `json:"date"`
    Explanation    string `json:"explanation"`
    MediaType      string `json:"media_type"`
    ServiceVersion string `json:"service_version"`
    Title          string `json:"title"`
    URL            string `json:"url"`
}
```

Then comes filtering. We would need to filter every image that we get. I would create wrapper for filtering function:
```go
type imageFilterFn(i Image) bool
```

Our `provider` would need to support that filtering functionality:

```go
type simpleProvider struct {
	filters []imageFilterFn // slice because we could add more than 1 filter
}
```

Finally, during `Get` method we check received image.

```go
func (s simpleProvider) Get(ctx context.Context, date time.Time) (*Image, error) { 
	...
	var i Image
	for _, f := range s.filters {
	    if ok := f(i); !ok {
	    	return nil, nil // image filtered out.
        }       	
    }
	return &i
}
```
Note: filters could be also passed as parameter of `Get` method. That would increase modularity - `task`
could contain list of filters to apply.

Described filter would likegi that:
```go
func newCopyrightFilter(c string) imageFilterFn {
	return func(i Image) bool {
		return i.Copyright == c
    }   
}
```