package handler

import (
	"encoding/json"
	"gogoapps/pkg/collector"
	"log"
	"net/http"
	"time"
)

// dateLayout is a time layout of receiving params.
const dateLayout = "2006-01-02"

// HTTPHandler is responsible for handling http traffic.
type HTTPHandler struct {
	collector collector.URLCollector
	logger    *log.Logger
}

// NewHTTPHandler returns new instance of HTTPHandler.
func NewHTTPHandler(collector collector.URLCollector, logger *log.Logger) *HTTPHandler {
	return &HTTPHandler{collector: collector, logger: logger}
}

// AddRoutes adds supported routes to mux.
func (h HTTPHandler) AddRoutes(mux *http.ServeMux) {
	mux.HandleFunc("/pictures", h.getPictures)
}

// getPicturesResponse holds requested urls.
type getPicturesResponse struct {
	Urls []string `json:"urls"`
}

// getPictures takes params from urls, calls Collector service and writes response back.
func (h HTTPHandler) getPictures(w http.ResponseWriter, r *http.Request) {
	h.logger.Println("start")
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	from := r.FormValue("from")
	to := r.FormValue("to")

	if from == "" {
		h.logger.Println("from is required")
		w.WriteHeader(http.StatusBadRequest)
		h.writeAndCheck(w, errorMsgToResponse("from is required"))
		return
	}
	if to == "" {
		h.logger.Println("to is required")
		w.WriteHeader(http.StatusBadRequest)
		h.writeAndCheck(w, errorMsgToResponse("to is required"))
		return
	}

	fromT, err := time.Parse(dateLayout, from)
	if err != nil {
		h.logger.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		h.writeAndCheck(w, errorMsgToResponse(err.Error()))
		return
	}

	toT, err := time.Parse(dateLayout, to)
	if err != nil {
		h.logger.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		h.writeAndCheck(w, errorMsgToResponse(err.Error()))
		return
	}

	urls, err := h.collector.Collect(fromT, toT)
	if err != nil {
		h.logger.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	resp := getPicturesResponse{urls}
	data, err := json.Marshal(resp)
	if err != nil {
		h.logger.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	h.writeAndCheck(w, data)
}

// httpError is a simple http error wrapper.
type httpError struct {
	Err string `json:"error"`
}

// errorMsgToResponse parses httpError.
func errorMsgToResponse(errMsg string) []byte {
	data, _ := json.Marshal(httpError{Err: errMsg})
	return data
}

func (h HTTPHandler) writeAndCheck(w http.ResponseWriter, content []byte) {
	if _, err := w.Write(content); err != nil {
		h.logger.Printf("failed to write content: %s", err)
	}
}
