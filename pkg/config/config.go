package config

import (
	"os"
	"strconv"
)

type Config struct {
	Port               string
	APIKey             string
	ConcurrentRequests int
}

const (
	portEnv               = "PORT"
	apiKeyEnv             = "API_KEY"
	concurrentRequestsEnv = "CONCURRENT_REQUESTS"
)

// defaultConfig holds default config values.
var defaultConfig = &Config{
	Port:               "8080",
	APIKey:             "DEMO_KEY",
	ConcurrentRequests: 5,
}

// LoadConfig loads config values from envs. If not present, default config values are set.
func LoadConfig() (*Config, error) {
	// it's way better to use viper here but to keep it
	// simple we will load it manually.

	c := defaultConfig

	port, ok := os.LookupEnv(portEnv)
	if ok {
		c.Port = port
	}
	apiKey, ok := os.LookupEnv(apiKeyEnv)
	if ok {
		c.APIKey = apiKey
	}
	concurrentRequests, ok := os.LookupEnv(concurrentRequestsEnv)
	if ok {
		cr, err := strconv.Atoi(concurrentRequests)
		if err != nil {
			return nil, err
		}
		c.ConcurrentRequests = cr
	}
	return c, nil
}
