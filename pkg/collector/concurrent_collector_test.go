package collector

import (
	"context"
	"gogoapps/pkg/mocks"
	"gogoapps/pkg/provider"
	"reflect"
	"runtime"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestConcurrentCollector(t *testing.T) {
	const limit = 1
	t.Run("RunAndClose", func(t *testing.T) {
		pr := &mocks.MockImageProvider{}

		cc := NewConcurrentCollector(limit, pr)

		gNum := runtime.NumGoroutine()
		cc.Run()
		require.Equal(t, gNum+limit, runtime.NumGoroutine())
		require.NotNil(t, cc.taskChan)

		cc.Close()
		require.True(t, cc.closed)
		time.Sleep(time.Second) // sleep for a while to catch proper NumGoroutine.
		require.Equal(t, gNum, runtime.NumGoroutine())
	})

	t.Run("WorkerFn", func(t *testing.T) {
		var (
			date         = time.Date(2021, 1, 1, 1, 0, 0, 0, time.UTC)
			ctx          = context.Background()
			wantedResult = &provider.Image{URL: "testURL"}
		)
		pr := &mocks.MockImageProvider{}
		pr.On("Get", mock.Anything, date).Return(wantedResult, nil)

		taskChannel := make(chan task, 1)
		resultChannel := make(chan result)
		taskChannel <- task{
			t:          date,
			ctx:        ctx,
			resultChan: resultChannel,
		}
		cc := NewConcurrentCollector(limit, pr)
		cc.wg.Add(1)
		go cc.workerFn(taskChannel)
		time.Sleep(time.Second)
		cc.finishChan <- struct{}{} // close workerFn
		result := <-resultChannel
		require.Equal(t, wantedResult, result.img)
		require.Nil(t, result.err)
	})

	t.Run("Collect", func(t *testing.T) {
		var (
			from = time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
			to   = time.Date(2020, 1, 3, 0, 0, 0, 0, time.UTC)
		)
		pr := &mocks.MockImageProvider{}

		cc := NewConcurrentCollector(limit, pr)
		pr.On("Get", mock.Anything, mock.Anything).Return(&provider.Image{URL: "testURL"}, nil)

		cc.Run()
		urls, err := cc.Collect(from, to)
		require.NoError(t, err)
		require.Equal(t, []string{"testURL", "testURL"}, urls)
	})
}

func Test_roundDate(t *testing.T) {
	tests := []struct {
		name string
		d    time.Time
		want time.Time
	}{
		{
			name: "TrimToDay",
			d:    time.Date(2020, 1, 1, 1, 1, 1, 1, time.UTC),
			want: time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
		},
		{
			name: "TrimToDay",
			d:    time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
			want: time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := roundDate(tt.d); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("roundDate() = %v, want %v", got, tt.want)
			}
		})
	}
}
