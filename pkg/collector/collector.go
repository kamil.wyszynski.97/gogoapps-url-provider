package collector

import "time"

// URLCollector is a interface that provides Urls.
type URLCollector interface {
	// Collect returns images' urls based on wanted date range.
	Collect(from, to time.Time) ([]string, error)
}
