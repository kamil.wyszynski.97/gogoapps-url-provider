package collector

import (
	"context"
	"errors"
	"gogoapps/pkg/provider"
	"log"
	"sync"
	"time"
)

// collectTimeout holds timeout for collect method.
// We don't want it to take too long or get stuck.
var collectTimeout = time.Second * 25

// ConcurrentCollector is concurrent implementation of URLCollector.
type ConcurrentCollector struct {
	limit      int // max amount of running workers.
	provider   provider.ImageProvider
	taskChan   chan task     // channel for sending task to workers.
	finishChan chan struct{} // channel for closing workers.
	logger     *log.Logger
	wg         *sync.WaitGroup
	closed     bool       // indicates if Close() method was called.
	mtx        sync.Mutex // takes care of accessing closed field.
}

// NewConcurrentCollector returns new instance of ConcurrentCollector.
func NewConcurrentCollector(limit int, provider provider.ImageProvider) *ConcurrentCollector {
	return &ConcurrentCollector{
		limit:      limit,
		provider:   provider,
		finishChan: make(chan struct{}, limit),
		wg:         &sync.WaitGroup{},
	}
}

// WithLogger sets logger.
func (c *ConcurrentCollector) WithLogger(logger *log.Logger) *ConcurrentCollector {
	c.logger = logger
	return c
}

// Run creates task channel and calls starts spawning workers.
func (c *ConcurrentCollector) Run() {
	taskChan := make(chan task, c.limit)
	c.taskChan = taskChan
	go c.spawnWorkers(taskChan)
}

// Close closes ConcurrentCollector. It sets closed field, sends messages
// to workers to stop working and waits for them to close.
func (c *ConcurrentCollector) Close() {
	c.mtx.Lock()
	c.closed = true
	c.mtx.Unlock()
	for i := 0; i < c.limit; i++ {
		c.finishChan <- struct{}{}
	}
	c.wg.Wait()
}

func (c *ConcurrentCollector) Collect(from, to time.Time) ([]string, error) {
	if c.taskChan == nil {
		return nil, errors.New("ConcurrentCollector needs to be Run")
	}
	urls := make([]string, 0)
	from = roundDate(from)
	to = roundDate(to)

	// set timeout for whole Collect method.
	ctx, cancel := context.WithTimeout(context.Background(), collectTimeout)
	defer cancel()

	// we want to know how much urls we want.
	wanted := int(to.Sub(from).Hours() / 24)
	resultChan := make(chan result, wanted)

	// generate tasks for workers.
	go func() {
		for t := from; t.Before(to); t = t.AddDate(0, 0, 1) {
			c.mtx.Lock()
			if c.closed {
				// we are closing taskChan here to avoid situation when
				// we write new task to closed channel.
				close(c.taskChan)
				return
			}
			c.mtx.Unlock()
			c.taskChan <- task{t, ctx, resultChan}
		}
	}()

	// loop for waiting for results.
	for {
		select {
		case r, ok := <-resultChan:
			if !ok { // channel was closed, return results.
				c.println("channel closed")
				return urls, nil
			}
			if r.err != nil {
				c.println("error: " + r.err.Error())
				// handle first seen error. We could join all of occurred errors but it's way easier that way :)
				return nil, r.err
			}
			urls = append(urls, r.img.URL)
			if len(urls) == wanted {
				return urls, nil
			}
		case <-ctx.Done():
			c.println("context done")
			return nil, errors.New("collect timeout")
		}
	}
}

// task represents task that worker needs to complete.
type task struct {
	t          time.Time       // date for worker to handle.
	ctx        context.Context // context of task.
	resultChan chan result     // where to send result.
}

// result is a result of fetching data.
type result struct {
	img *provider.Image // result of task.
	err error           // error of task.
}

// roundDate trims time.Time to day.
func roundDate(date time.Time) time.Time {
	return time.Date(date.Year(), date.Month(), date.Day(), 0, 0, 0, 0, date.Location())
}

// spawnWorkers spawns limit amount of worker goroutines.
func (c *ConcurrentCollector) spawnWorkers(taskChannel chan task) {
	for i := 0; i < c.limit; i++ {
		c.wg.Add(1)
		go c.workerFn(taskChannel)
	}
	c.println("workers spawned")
}

// workerFn is responsible for fetching task from taskChannel
// and performing it using provider.
func (c *ConcurrentCollector) workerFn(taskChannel chan task) {
	defer c.wg.Done()
	for {
		select {
		case task, ok := <-taskChannel:
			if !ok {
				// task channel closed - finish work.
				return
			}
			c.println("task received")
			img, err := c.provider.Get(task.ctx, task.t)
			c.println("task done")
			task.resultChan <- result{img, err}
		case <-c.finishChan:
			c.println("worker finishes")
			return
		}
	}
}

// println prints given values if logger is set.
func (c *ConcurrentCollector) println(v ...interface{}) {
	if c.logger == nil {
		return
	}
	c.logger.Println(v...)
}
