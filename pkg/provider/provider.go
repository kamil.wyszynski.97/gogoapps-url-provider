package provider

import (
	"context"
	"time"
)

// Image represents image.
type Image struct {
	// URL hold url to .jpg image.
	URL string `json:"url"`
	// There are plenty of other fields but we don't need them for now.
}

// ImageProvider is an interface that provides access to Images.
type ImageProvider interface {
	// Get returns Image for given date.
	Get(ctx context.Context, date time.Time) (*Image, error)
}
