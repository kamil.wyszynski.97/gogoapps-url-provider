package provider

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

const (
	// dateLayout is date format that nasa API supports.
	dateLayout = "2006-01-02"

	// nasaURL is a  NASA API endpoint.
	nasaURL = "https://api.nasa.gov/planetary/apod?api_key=%s&date=%s"
)

// NasaImageProvider implements ImageProvider for NASA APOD collection.
type NasaImageProvider struct {
	httpClient *http.Client
	apiKey     string
}

// NewNasaImageProvider returns new instance of NasaImageProvider.
func NewNasaImageProvider(httpClient *http.Client, apiKey string) *NasaImageProvider {
	return &NasaImageProvider{
		httpClient: httpClient,
		apiKey:     apiKey,
	}
}

func (n NasaImageProvider) Get(ctx context.Context, date time.Time) (*Image, error) {
	dateStr := date.Format(dateLayout)

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, fmt.Sprintf(nasaURL, n.apiKey, dateStr), nil)
	if err != nil {
		return nil, err
	}
	resp, err := n.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	var image Image
	if err := json.NewDecoder(resp.Body).Decode(&image); err != nil {
		return nil, err
	}
	return &image, err
}
