package mocks

import (
	"context"
	"gogoapps/pkg/provider"
	"time"

	"github.com/stretchr/testify/mock"
)

type MockImageProvider struct {
	mock.Mock
}

func (m *MockImageProvider) Get(ctx context.Context, date time.Time) (*provider.Image, error) {
	args := m.Called(ctx, date)
	return args.Get(0).(*provider.Image), args.Error(1)
}
