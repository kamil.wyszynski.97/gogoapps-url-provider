package main

import (
	"fmt"
	"gogoapps/pkg/collector"
	"gogoapps/pkg/config"
	"gogoapps/pkg/handler"
	"gogoapps/pkg/provider"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	logger := log.Default()
	cfg, err := config.LoadConfig()
	if err != nil {
		logger.Panic(err)
	}
	mux := http.NewServeMux()

	httpClient := http.DefaultClient
	p := provider.NewNasaImageProvider(httpClient, cfg.APIKey)
	c := collector.NewConcurrentCollector(cfg.ConcurrentRequests, p).WithLogger(logger)
	c.Run()

	h := handler.NewHTTPHandler(c, logger)

	h.AddRoutes(mux)

	termChan := make(chan os.Signal, 1)
	signal.Notify(termChan, syscall.SIGINT, syscall.SIGTERM)

	srv := http.Server{Handler: mux, Addr: fmt.Sprintf(":%s", cfg.Port)}
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
	logger.Print("Server Started")
	<-termChan
	logger.Print("Shutdown")
	c.Close()
}
